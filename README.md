# AnkiDecks

## Description

This is a collection of several Anki-Decks for the B.Sc of Computer Science at the RWTH. They are intended for my and my friends personal use only,
but whoever gets access to them may use them in any way they want. I do not claim any factual accuracy and won't be responsible for any
wrong knowledge you may acquire through the use of those decks. Have fun!

## Usage

### Subdecks:
Every deck is partitioned into subdecks. Those subdecks are updated more often than the superdeck, as they are intended for parallel
editing because current Anki-Deck formats do not allow parallel workflows at the moment. If you want to be up-to-date, import
those, they will find their way into the superdeck.

### Superdecks:
Superdecks are the "releases" of the Anki-Decks. They are updated periodically using the aforementioned subdecks but might not be
as up-to-date. Import those before importing subdecks.

## Quiz-Cards
![Screenshot](readmePics/question_answer.png)
You might come along a Deck and think "Why does it have 60 cards for one lecture?!". Then you go through it anyway and notice that it went surprisingly quick. The reason is simple: I decided to try out something Anki is not meant to do: Quiz cards. It's simple: Multiple choice questions with answers in a pseudo-randomized order. They are not checked automatically, but when you click on "view solution", the colors indicate weather a given statement is a correct answer to the question or not. There can be all right and all wrong cards. It's just more fun this way. You choose weather it went well. No automation what-so-ever. The reason you see so many cards is that quiz-cards generate about 5 cards each. The generated cards each have a different order and contain different answers. This prevents mindless pattern matching and "simulates" actual randomness. In addition, every quiz has just one new card per day, which is the reason that most cards will get skipped when you do the deck for the first time and you wont have to do all 60 cards Anki shows you.

However: Beware. There might be some bug i didn't notice and the answers could be scrambled. If you don't agree with my card, look it up so you don't learn mere rubbish. It would be nice if you sent every error you encounter to fredrik.konrad@rwth-aachen.de.

## Contribute

Anyone can contribute to those decks or add their own to grow this collection. If you do so, you accept that anyone is allowed to 
modify and share this work however they please. If you intend to be named in the credits below, make sure to provide your name.

### New decks:
Submit a pull request via git or message me (Fredrik Konrad) via Fredrik.Konrad@rwth-aachen.de. They will, of course, have to live up
to the other deck's standards and cannot contain any images that you don't have the rights of (e.g. screenshots from scripts/slides).
If you want to publish your deck anyway, i advise you to recreate those screenshots by yourself.

### New Cards:
If you add new cards to an existing deck, you have to make sure that only you edit the deck at this time. As we are doing it in a
small team at the moment, we don't have any "Mutex"-mechanism, we just let each other know. Just submit an issue via git containing
all the necessary information for the card and it will be added in a future release (provided its not misinformation).

### New Visualisations:
Visualizations in SVG form are currently created in Draw.io. I would advise you to keep to this style. Rasterized images (.png,.jpeg,...)
are only welcome in exceptions, as they tend to blow up in file size rather quickly and don't look as good on every display. If there
is no vectorized alternative (e.g. it has to be a photo of a given scientist) they will be accepted. If you think a card could need
a visualization and want one created, submit an issue describing what you have in mind.

### Mistakes:
As we are all humans, those decks might be lacking some information, can just be plain wrong or hard to understand. In those cases,
submit an issue and it will be taken care of, or you might even take care of it yourself :)

### Suggested Add-Ons for contributing:
Anki offers many Add-Ons that add functionality and make it possible or easier to create more readable cards. I will here provide a "personal hall of fame" and include the download code to each. Even though those Add-Ons can only be installed on PC, they only change the CSS-Style of any card and thus have the same effects on every other device. If this is not the case, contact me. If you cannot install them, make sure you run a supported version of Anki 2.1. As I did not use them in any pack, you can also contribute by updating bullet point lists.

- Mini Format Pack (For bullet point lists. As you can see, useful)
	- Code: 295889520
- Resize images in editor
	- Code: 1103084694

## Credits

Creators of the Anki-Packages and other resources:
Fredrik Konrad,
Laura Drescher-Manaa,
Patrick Chrestin, 
Jennifer Drew
Lukas Liesenberg

Professors responsible for the scripts used to create those resources:
Prof. Dr-Ing. Klaus Wehrle,
Dr. rer. nat. Dirk Thißen,
Prof. Dr. Erich Grädel,
Prof. Dr. Gerhard Hiss,
Prof. Dr. Martin Grohe,
Prof. Dr. Astrid Rosenthal-von der Pütten
Prof. Dr-Ing Ulrike Meyer
Prof. Dr. rer. pol. Rüdiger von Nitzsch
